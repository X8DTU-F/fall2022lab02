// Thomas Proctor | 2136365 | :D
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args) { 
        Bicycle[] bikeRack = new Bicycle[4];
        bikeRack[0] = new Bicycle("Ford", 4, 10.4);
        bikeRack[1] = new Bicycle("Tesla", 6, 50.2);
        bikeRack[2] = new Bicycle("Audi", 5, 36.3);
        bikeRack[3] = new Bicycle("Honda", 3, 3.7);

        for (int i=0; i < bikeRack.length; i++) {
            System.out.println(bikeRack[i]);
        }

    }
    
}
