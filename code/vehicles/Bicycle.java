// Thomas Proctor | 2136365 | :D
package vehicles;
public class Bicycle {
    String manufacturer;
    int numberGears;
    double maxSpeed;
    

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    } 

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int numberGears() {
        return this.numberGears;
    }

    public double maxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        String returnString = "Manufacturer: " + this.manufacturer + ", Number of gears: " + this.numberGears + ", Max Speed: " + this.maxSpeed();
        return returnString;
    }
}